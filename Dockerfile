FROM python:3.7

ENV DEBIAN_FRONTEND "noninteractive"

ENV GO_IPFS_STABLE_VERSION "0.9.1"
ENV KUBO_VERSION "0.24.0"
ENV IPFS_DIST_OS "linux"
ENV FS_MIGRATE_VERSION "2.0.2"
ENV GALACTEEK_VENV "/galacteek/venv"
ENV MINICONDA_DIST "Miniconda3-py37_4.10.3-Linux-x86_64.sh"
ENV APPDIR_GLOBAL "/galacteek/appdir"

ENV NOTBIT_DIST_URL "https://gitlab.com/api/v4/projects/27627112/packages/generic/notbit/0.7/notbit.tar.gz"

COPY [".gitlab/miniconda-setup.sh", "."]

RUN apt-get update
RUN apt-get \
  -o Dpkg::Options::="--force-confold" \
  -o Dpkg::Options::="--force-confdef" \
  -q -y --force-yes install \
  libgl1 xinit x11-xserver-utils x11-utils libxcb-xkb1 \
  libxkbcommon-x11-0 libzbar0 tor libsystemd0 autoconf automake gcc \
  libpulse-mainloop-glib0 libpulse0 libnss3 \
  libxtst6 libasound2 libxkbcommon0 libx11-xcb1 \
  flatpak flatpak-builder wget curl bash \
  libfuse2 \
  git libserd-0-0 libserd-dev \
  trickle

RUN apt-get \
  -o Dpkg::Options::="--force-confold" \
  -o Dpkg::Options::="--force-confdef" \
  -q -y --force-yes install --no-install-recommends \
  libqt5multimedia5 libqt5multimediaquick5

RUN chmod u+s /usr/bin/bwrap

# Flatpak
RUN flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

RUN python -m pip install --upgrade pip

RUN wget https://dist.ipfs.tech/go-ipfs/v${GO_IPFS_STABLE_VERSION}/go-ipfs_v${GO_IPFS_STABLE_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz

RUN wget https://dist.ipfs.tech/kubo/v${KUBO_VERSION}/kubo_v${KUBO_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz && \
	tar -xvf kubo_v${KUBO_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz && \
	rm kubo_v${KUBO_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz

RUN wget https://dist.ipfs.tech/fs-repo-migrations/v${FS_MIGRATE_VERSION}/fs-repo-migrations_v${FS_MIGRATE_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz

RUN tar -xzvf \
  go-ipfs_v${GO_IPFS_STABLE_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz && \
  cp go-ipfs/ipfs /usr/bin/ipfs-${GO_IPFS_STABLE_VERSION} && \
  rm go-ipfs_v${GO_IPFS_STABLE_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz

RUN cp kubo/ipfs /usr/bin/ipfs-${KUBO_VERSION}
RUN cp kubo/ipfs /usr/bin
RUN tar -xzvf fs-repo-migrations_v${FS_MIGRATE_VERSION}_${IPFS_DIST_OS}-amd64.tar.gz
RUN cp fs-repo-migrations/fs-repo-migrations /usr/bin

RUN mkdir /galacteek

RUN wget https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/requirements.txt
RUN wget https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/requirements-dev.txt
RUN wget https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/requirements-extra-markdown.txt
RUN wget https://gitlab.com/galacteek/galacteek/-/raw/pimp-my-dweb/requirements-ui-pyqt-5.15.txt

RUN pip install wheel
RUN pip install -r requirements-dev.txt
RUN pip install -r requirements.txt
RUN pip install -r requirements-extra-markdown.txt
RUN pip install -r requirements-ui-pyqt-5.15.txt

RUN curl -o /galacteek/${MINICONDA_DIST} \
  "https://repo.anaconda.com/miniconda/${MINICONDA_DIST}"

RUN bash /galacteek/${MINICONDA_DIST} -b -p "${APPDIR_GLOBAL}"/usr -f

RUN bash ./miniconda-setup.sh ${APPDIR_GLOBAL}

RUN curl -o /galacteek/appimagetool \
  "https://github.com/AppImage/AppImageKit/releases/download/continuous/appimagetool-x86_64.AppImage"
RUN chmod +x /galacteek/appimagetool

RUN wget ${NOTBIT_DIST_URL}
RUN tar -C /usr/bin -xvf notbit.tar.gz
RUN tar -C ${APPDIR_GLOBAL}/usr/bin -xvf notbit.tar.gz

RUN git clone https://github.com/rdfhdt/hdt-cpp && \
  cd hdt-cpp && ./autogen.sh && ./configure && make install

CMD ipfs init

ENTRYPOINT []
