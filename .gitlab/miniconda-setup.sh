#!/bin/bash

MINI=$1

source $MINI/usr/bin/activate

python -m pip install --upgrade pip
conda install -y -c conda-forge libxkbcommon pip \
    xcb-util xcb-util-wm xcb-util-image xcb-util-renderutil \
    xcb-util-keysyms \
    fontconfig libjpeg-turbo openssl
conda install -y --no-deps libxcb

conda remove certifi
conda install certifi

# pip install -r requirements.txt
pip install -r requirements-extra-markdown.txt
pip install -r requirements-ui-pyqt-5.15.txt

cp /usr/bin/ipfs $MINI/usr/bin
cp /usr/bin/fs-repo-migrations $MINI/usr/bin

pushd "$MINI/usr"
rm -rf share/aclocal
rm -rf share/libtool
rm -rf share/gtk-2.0
rm -rf share/icu
rm -rf share/X11
rm -rf share/themes
rm -rf share/gir-1.0
popd
